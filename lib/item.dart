import 'package:flutter/material.dart';

class Item{

  Item({this.country, this.name, /*this.web*/});

  final String country;
  final String name;
  /*final String web;*/

  factory Item.fromJson(Map<String, dynamic> jsonData){
    return Item(country: jsonData['country'], name: jsonData['name'], /*web: jsonData['web_pages'],*/);
  }
}