import 'package:api_2nd_practice/item.dart';
import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {
  ListItem(this.item);

  final Item item;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 45,
      margin: EdgeInsets.all(10),
      child: Column(children: [
        Text(item.name),
        /*subtitle: Text(item.web),*/
        Text(item.country),
      ]),
    );
  }
}
