import 'package:http/http.dart' as http;

class HttpService{

  HttpService(this.query);

  final String query;
  // String usersUrl = 'http://universities.hipolabs.com/search?country=$query';

  Future<dynamic> getItems() async{
    var response = await http.get('http://universities.hipolabs.com/search?country=$query');
    return response.body;
  }
}