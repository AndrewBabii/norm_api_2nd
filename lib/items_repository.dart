import 'package:api_2nd_practice/http_service.dart';
import 'package:api_2nd_practice/item.dart';
import 'dart:convert';


class ItemRepository{
  Future<List<Item>> getItems(String query) async{
    List<Item> items = [];

    var response = await HttpService(query).getItems();
    List<dynamic> jsonData = json.decode(response);
    // print(jsonData[2]);

    for(int i = 0; i < jsonData.length; i++){
      items.add(Item.fromJson(jsonData[i]));
    }
    return items;
  }
}