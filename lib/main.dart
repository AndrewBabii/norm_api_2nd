import 'package:api_2nd_practice/items_provider.dart';
import 'package:api_2nd_practice/items_repository.dart';
import 'package:api_2nd_practice/listItem.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:api_2nd_practice/item.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ItemsProvider.instance,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
/*
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {*/
  final textController = TextEditingController();

/*  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }*/
  List<Item> items = [];




  @override
  Widget build(BuildContext context) {

    var itemData = Provider.of<ItemsProvider>(context);
    final listItems = itemData.items;
    return Scaffold(
      appBar: AppBar(
        title: Text('widget.title'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(8),
            child: TextField(
              controller: textController,
              decoration: InputDecoration(labelText: 'type your query here'),
            ),
          ),
          FlatButton(
            child: Text('Then tap here'),
            onPressed: () {
              String query = textController.text;
              itemData.getItems(query);

            },
          ),
          listItems.length > 0
              ? Container(
                  color: Colors.deepOrangeAccent,
                  height: 300,
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      print('bilder.build');
                      return ListItem(listItems[index]);
                    },
                    itemCount: listItems.length,
                  ),
                )
              : Text('nothing to show'),
        ],
      ),
    );
  }
}

/*

ChangeNotifierProvider(
create: (ctx) => ItemsProvider.instance,*/
